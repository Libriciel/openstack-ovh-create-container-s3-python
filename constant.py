import logging
from os import environ as env

OVH_END_POINT = "ovh-eu"
OVH_REGIOSNLIST = ["SBG", "GRA"]

OS_AUTH_URL = "https://auth.cloud.ovh.net/v3"
OS_USER_DOMAIN_NAME = "Default"


# Obtenu lors de la création des crédentials via l'API OVH
# Exemple d'API :
# https://api.ovh.com/createToken/index.cgi?GET=/cloud/project/OS_PROJECT_ID/storage/*&POST=/cloud/project/OS_PROJECT_ID/storage/*&GET=/cloud/project/OS_PROJECT_ID/storage&POST=/cloud/project/OS_PROJECT_ID/storage&GET=/cloud/project/OS_PROJECT_ID/user/*


ovhinfo = {
    "endpoint": OVH_END_POINT,
    "application_key": env["OVH_APPLICATION_KEY"],
    "application_secret": env["OVH_APPLICATION_SECRET"],
    "consumer_key": env["OVH_CONSUMER_KEY"],
    "project_id": env["OS_PROJECT_ID"],
    "region_list": OVH_REGIOSNLIST,
}

swiftinfo = {}

for region in OVH_REGIOSNLIST:
    infos = {
        "_authurl": OS_AUTH_URL,
        "_auth_version": "3",
        "_user": env["OS_USERNAME"],
        "_key": env["OS_PASSWORD"],
        "_os_options": {
            "user_domain_name": OS_USER_DOMAIN_NAME,
            "project_domain_name": "Default",
            "project_name": env["OS_PROJECT_NAME"],
            "region_name": region,
        },
    }
    swiftinfo[region] = infos

level = logging.INFO
