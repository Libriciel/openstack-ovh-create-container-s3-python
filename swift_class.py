import logging

import swiftclient


class SwiftClass:
    def __init__(self, swiftinfo):
        self.conn = swiftclient.Connection(
            authurl=swiftinfo["_authurl"],
            user=swiftinfo["_user"],
            key=swiftinfo["_key"],
            os_options=swiftinfo["_os_options"],
            auth_version=swiftinfo["_auth_version"],
        )

    def get_account(self):
        logging.debug("[get_account] - Retourne la connexion swift")
        return self.conn.get_account()

    def head_container(self, containername):
        logging.debug("[head_container] - iste des containers swift")
        return self.conn.head_container(containername)

    def getWriteAcl(self, info):
        logging.debug(
            f"[getWriteAcl] - Regarde si le champs x-container-write est vide {info}"
        )
        if "x-container-write" in info:
            logging.debug("[getWriteAcl] - le champs x-container-write n est pas vide")
            return info["x-container-write"]

        return ""

    def getReadAcl(self, info):
        logging.debug(
            f"[getReadAcl] - Regarde si le champs x-container-write est vide {info}"
        )
        if "x-container-read" in info:
            logging.debug("[getReadAcl] - le champs x-container-write n est pas vide")
            return info["x-container-read"]

        return ""

    def put_container(self, name):
        logging.debug(f"[put_container] - Creation du container {name}")
        result = self.conn.put_container(name)
        logging.debug(f"[put_container] - Retour creation du container {result}")

    def post_container(self, name, metadata):
        logging.debug(f"[post_container] - Creation du container {name}")
        result = self.conn.post_container(name, metadata)
        logging.debug(
            f"[post_container] - Retour changement des metadata du container {result}"
        )
