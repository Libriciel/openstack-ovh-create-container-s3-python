import logging


# Fonction pour definir le nom d'un container
def createnamecontainer(name, index, region):
    logging.debug(
        f"[createnamecontainer] - paramètres fournis dans createnamecontainer {name} - {index} - {region}"
    )
    return str.lower(name) + "-" + str(index) + "-" + str.lower(region)


def cretelistcontainername(regionlist, number, bucketname):
    listnamecontainer = []
    for region in regionlist:
        logging.debug(
            f"[cretelistcontainername] - Region pour le nom du contain er {region}"
        )
        for index in range(0, int(number)):
            logging.debug(
                f"[cretelistcontainername] - numéro index pour le nom du container {index}"
            )
            listnamecontainer.append(
                createnamecontainer(bucketname, index, str.lower(region))
            )

    return listnamecontainer


def listcontainertocreate(listcontainerexist, listcontainername):
    listcontainertocreate = []
    for containertocreate in listcontainername:
        logging.debug(
            f"[listcontainertocreate] - Container name to verify : {containertocreate}"
        )
        foundit = False
        for containerexistinfo in listcontainerexist:
            logging.debug(f"[listcontainertocreate] - Compare to {containerexistinfo}")
            if containerexistinfo["name"] == containertocreate:
                region = containertocreate.split("-")[2]
                logging.debug(f"[listcontainertocreate] - Region : {region}")
                if str.upper(region) == str.upper(containerexistinfo["region"]):
                    logging.debug("[listcontainertocreate] - C'est la même region")
                    foundit = True

        if not foundit:
            listcontainertocreate.append(
                {
                    "name": containertocreate,
                    "region": str.upper(containertocreate.split("-")[2]),
                }
            )

    return listcontainertocreate


def createcontainer(listcontainertocreate, ovhclient):
    for info in listcontainertocreate:
        logging.debug(f"[createcontainer] - We need to create : {info}")
        ovhclient.createcontainer(info["name"], str.upper(info["region"]))


def deleteallcontainer(listallcontainer, listcontainertodelete, ovhclient):
    for containerinfo in listallcontainer:
        logging.debug(f"[deleteallcontainer] - Info sur le container {containerinfo}")
        if containerinfo["name"] in listcontainertodelete:
            logging.debug("[deleteallcontainer] - Suppression du container")
            ovhclient.deletecontainer(containerinfo["id"])


def getlistcontainertowork(listallcontainer, listname):
    listcontainertowork = []
    for containerinfo in listallcontainer:
        logging.debug(f"[getlistcontainertowork] - {containerinfo}")
        if containerinfo["name"] in listname:
            logging.debug(
                "[getlistcontainertowork] - Le nom est présent dans `listname`"
            )
            listcontainertowork.append(containerinfo)

    return listcontainertowork


def deleteuser(ovhclient, listusers, listcontainername):
    for user in listusers:
        logging.debug(f"[deleteuser] - Information sur user : {user}")
        if user["description"] in listcontainername:
            logging.debug("[deleteuser] - Suppresion de l utilisateur")
            ovhclient.deleteuser(str(user["id"]))


def getuserid(listalluser, username):
    for user in listalluser:
        logging.debug(f"[getuserid] - Information sur user : {user}")
        if user["username"] == username:
            logging.debug(
                f"[getuserid] - {user['username']} est equivalenet a {username}"
            )
            return user["id"]
