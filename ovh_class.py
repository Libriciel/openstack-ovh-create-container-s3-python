import logging
import time

import ovh


class APIOVHcloud:
    project_id = 0

    def __init__(self, ovhinfo):
        self.client = ovh.Client(
            endpoint=ovhinfo["endpoint"],
            application_key=ovhinfo["application_key"],
            application_secret=ovhinfo["application_secret"],
            consumer_key=ovhinfo["consumer_key"],
        )
        self.project_id = ovhinfo["project_id"]

    def recupliststorage(self):
        api = "/cloud/project/" + self.project_id + "/storage"
        logging.debug(f"[recupliststorage] - api appellee : {api}")
        result = self.client.get(api)
        return result

    def createcontainer(self, name, region):
        data = {"archive": False, "containerName": name, "region": str.upper(region)}

        logging.debug(f"[createcontainer] - contenu data : {data}")

        api = "/cloud/project/" + self.project_id + "/storage"
        logging.debug(f"[createcontainer] - api appellee : {api}")
        result = self.client.post(api, **data)
        return result

    def deletecontainer(self, containerid):
        api = "/cloud/project/" + self.project_id + "/storage/" + containerid
        logging.debug(f"[deletecontainer] - api appellee : {api}")
        self.client.delete(api)

    def recupuserobjectstore_all(self):
        api = "/cloud/project/" + self.project_id + "/user/"
        logging.debug(f"[recupuserobjectstore_all] - api appellee : {api}")
        userallinfo = self.client.get(api)
        userid = ""
        logging.debug(
            f"[recupuserobjectstore_all] - Information sur tous les utilisateurs : {userallinfo}"
        )
        for userinfo in userallinfo:
            for roles in userinfo["roles"]:
                logging.debug(f"[recupuserobjectstore_all] - role de user : {roles}")
                if "objectstore_all" in roles["permissions"]:
                    userid = userinfo["id"]

        return userid

    def getopenrc(self, userid, region):
        data = {"region": str.upper(region), "version": "v3"}
        logging.debug(f"[getopenrc] - contenu data : {data}")
        api = "/cloud/project/" + self.project_id + "/user/" + str(userid) + "/openrc"
        logging.debug(f"[getopenrc] - api appellee : {api}")
        useropenrc = self.client.get(api, **data)
        return useropenrc

    def createuserforcontainer(self, containerid, description):
        data = {"description": description, "right": "all"}
        logging.debug(f"[createuserforcontainer] - contenu data : {data}")
        api = "/cloud/project/" + self.project_id + "/storage/" + containerid + "/user"
        logging.debug(f"[createuserforcontainer] - api appellee : {api}")
        result = self.client.post(api, **data)

        logging.debug(
            "[createuserforcontainer] - On attend que le statut de l'utilisateur soit OK"
        )

        while self.getStatusUser(result["id"]) != "ok":
            time.sleep(10)

        return result

    def deleteuser(self, userid):
        api = "/cloud/project/" + self.project_id + "/user/" + str(userid)
        logging.debug(f"[deleteuser] - api appellee : {api}")
        print(self.client.delete(api))

    def getalluser(self):
        api = "/cloud/project/" + self.project_id + "/user"
        logging.debug(f"[getalluser] - api appellee : {api}")
        return self.client.get(api)

    def getStatusUser(self, userid):
        api = "/cloud/project/" + self.project_id + "/user/" + str(userid)
        logging.debug(f"[getStatusUser] - api appellee : {api}")
        status = self.client.get(api)
        logging.debug(f"[getStatusUser] - user status : {status}")
        return status["status"]

    def s3Credentials(self, userid):
        api = (
            "/cloud/project/"
            + self.project_id
            + "/user/"
            + str(userid)
            + "/s3Credentials"
        )
        logging.debug(f"[getStatusUser] - api appellee : {api}")
        info = self.client.call("POST", api, None, True)
        logging.debug(f"[getStatusUser] - user s3 credentials : {info}")
        return info

    def gets3Credentials(self, userid):
        api = (
            "/cloud/project/"
            + self.project_id
            + "/user/"
            + str(userid)
            + "/s3Credentials"
        )
        logging.debug(f"[gets3Credentials] - api appellee : {api}")
        info = self.client.get(api)
        logging.debug(f"[gets3Credentials] - user s3 credentials : {info}")
        return info[0]
