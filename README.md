# openstack-ovh-create-container-s3-python

Ce projet permet de créer un container Openstack via les API d'OVHcloud et d'Openstack avec :
* un utilisateur dédié au container dont les droits sont lecture et écriture
* l'ajout des API S3
* la création du container `+segements` pour réaliser le multipart
* la création du bucket sur les région `SBG` et `GRA`


Le programme est écrit en Python.

## Préparation

Vous avez besoin de récupérer les informations ci-dessous pour exécuter le programme.
Dans le fichier `openrc.sh` de votre projet Openstack vous devez récupérer les valeurs des variables `OS_USERNAME`, `OS_PROJECT_ID` et `OS_PROJECT_NAME`. Vous pouvez les mettre dans un fichier `.env`. Dans ce fichier, vous devez renseigner le mot de passe de votre utilisateur. Cette information sera à affecter à la variable `OS_PASSWORD`.
Exemple :
```text
OS_PROJECT_ID="identifiantduprojet"
OS_USERNAME="nomdelutilisateur"
OS_PASSWORD="motdepassedelutilisateur"
OS_PROJECT_NAME="nomduprojetdanslefichieropenrc"
```

A présent, il est nécessaire de créer un accès aux API d'OVHcloud. Pour cela vous devez aller sur l'URL `https://api.ovh.com/createToken/index.cgi`. Pour connaître les éléments dont nous avons besoin, voici l'URL pré-formatée :
`https://api.ovh.com/createToken/index.cgi?GET=/cloud/project/IDENTIFIANTDUPROJET/storage/*&GET=/cloud/project/IDENTIFIANTDUPROJET/storage&GET=/cloud/project/IDENTIFIANTDUPROJET/user/*&GET=cloud/project/IDENTIFIANTDUPROJET/user&POST=/cloud/project/IDENTIFIANTDUPROJET/storage&POST=/cloud/project/IDENTIFIANTDUPROJET/storage/*&POST=/cloud/project/IDENTIFIANTDUPROJET/user/*&DELETE=/cloud/project/IDENTIFIANTDUPROJET/storage/*&DELETE=/cloud/project/IDENTIFIANTDUPROJET/user/*]`

Dans cette URL vous devez remplacer `IDENTIFIANTDUPROJET` par le contenu de la variable `OS_PROJECT_ID`.

Nous vous invitons à limiter l'accès aux API à des IP publiques spécifiques.

Le résultat de la requête vous fournira trois informations, `application key`, `application secret` et `consumer key`.
Vous devez renseigner ces informations dans votre fichier `.env` :
```text
OVH_APPLICATION_KEY="application key"
OVH_APPLICATION_SECRET="application secret"
OVH_CONSUMER_KEY="consumer key"
```

## Programme

### Paramètres pris en compte

Le fichier `main.py` est le programme à lancer. Il prend en paramètres :
* `-b` ou `--bucket-name=` le nom du bucket que vous souhaitez créer. Il sera nommé ainsi `nom_du_bucket_passé_en_paramètre-index-région`
* `-n` ou `number=` le nombre de container/bucket à créer

Par exemple l'appel suivant `pyhton3 main.py -n 1 -b libriciel` créera les buckets `libriciel-0-sbg` et `libriciel-0-sbg`

On peut aussi définir le niveau de log avec `--log=DEBUG`. Par défaut, le niveau est à `INFO`.

### Sortie

En plus de quelques informations de log, les dernières lignes sont du json formaté.
Un élément correspond à un bucket. Pour chaque élément on retrouve systématiquement et dans cet ordre :
* acces_key
* le nom du bucket
* l'endpoint pour accéder au bucket
* la region (en majuscule telle qu'elle est chez OVH)
* secret



## Exécution

### Lancement du programme depuis le docker

Vous avez la possibilité de récupérer le fichier `docker-compose.yml` depuis le git du projet ([https://gitlab.libriciel.fr/outils/openstack-ovh-create-container-s3-python/-/blob/master/docker-compose.yml])
```bash
version: '3.7'
services:
    ovh:
        container_name: ${HOSTNAME:-ovh-create-container-s3}
        hostname: ${HOSTNAME:-ovh-create-container-s3.libriciel.fr}
        image: libriciel:ovh-container-s3
        environment:
            OVH_APPLICATION_KEY: ${OVH_APPLICATION_KEY:-applicationkey}
            OVH_APPLICATION_SECRET: ${OVH_APPLICATION_SECRET:-applicationsecret}
            OVH_CONSUMER_KEY: ${OVH_CONSUMER_KEY:-consumerkey}
            OS_PROJECT_ID: ${OS_PROJECT_ID:-123456789012314567890}
            OS_USERNAME: ${OS_USERNAME:-username}
            OS_PASSWORD: ${OS_PASSWORD:-password}
            OS_AUTH_URL: ${AUTH_URL:-https://auth.cloud.ovh.net/v3}
            OS_PROJECT_NAME: ${OS_PROJECT_NAME:-1234567890}
```


Lancer le docker :
```bash
docker-compose up -d
```

Ensuite on appelle le programme ainsi :
```bash
docker exec -it ovh-create-container-s3 python3 main.py -n nombre_de_bucket_par_regions -b nom_du_bucket
```


### Lancement depuis les sources

#### Pré-requis
Le programme est écrit en Python 3.
Sur une distribution Ubuntu 20.04 les programmes `python3-pip` et `python3-swiftclient` sont nécessaires. De même, il faut importer la librairie OVHcloud avec la commande `pip3 install ovh`.


#### Execution

On récupère le projet
```bash
git clone https://gitlab.libriciel.fr/outils/openstack-ovh-create-container-s3-python.git
```

On lance le programme ainsi : `python3 main.py -n nombre_de_bucket_par_regions -b nom_du_bucket`