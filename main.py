import getopt
import json
import logging
import sys

import myfonctions
from constant import level, ovhinfo, swiftinfo
from ovh_class import *
from swift_class import *

formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")


args = ["bucket-name=", "number=", "log="]
shortopts = "b:n:"

numberbucket = 1
bucketname = "libriciel"

arguments_list, arguments_notgood = getopt.getopt(sys.argv[1:], shortopts, args)


for opt, arg in arguments_list:
    if opt in ("-b", "--bucket-name"):
        bucketname = arg
    elif opt in ("-n", "--number"):
        numberbucket = arg
    elif opt in ("--log"):
        level = getattr(logging, arg.upper(), logging.INFO)


logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=level)

logging.info(f"Nom du bucket : {bucketname} et le nombre : {numberbucket}")


logging.debug(ovhinfo)

ovhcloud = APIOVHcloud(ovhinfo)


swiftovh = {}
for key in swiftinfo:
    swiftovh[key] = SwiftClass(swiftinfo[key])

listcontainerexist = ovhcloud.recupliststorage()
logging.debug(f"Liste des container existant {listcontainerexist}")


logging.info("Creation liste des noms des buckets par region")
listnamecontainer = myfonctions.cretelistcontainername(
    ovhinfo["region_list"], numberbucket, bucketname
)
logging.debug(f"Liste des nom des containers suivant l'algorithme {listnamecontainer}")

logging.info("On ne cree pas les container déjà existant")
listcontainertocreate = myfonctions.listcontainertocreate(
    listcontainerexist, listnamecontainer
)
logging.debug(f"Liste des noms des containers à creer {listcontainertocreate}")

logging.info(f"Creation des {numberbucket}  containers dans chaque région")
myfonctions.createcontainer(listcontainertocreate, ovhcloud)

# logging.info('Delete all container')
# myfonctions.deleteallcontainer(listcontainerexist, listnamecontainer, ovhcloud)

logging.info("Recupere la liste des users")
listalluser = ovhcloud.getalluser()
logging.debug(f"Liste des utilisateurs existant {listalluser}")
# myfonctions.deleteuser(ovhcloud, listalluser, listnamecontainer)


listcontainerexist = ovhcloud.recupliststorage()
listcontainertowork = myfonctions.getlistcontainertowork(
    listcontainerexist, listnamecontainer
)
logging.debug(f"Liste des container sur lesquels on travaille {listcontainertowork}")

logging.info("Check if user read-write on container")

informationsglobales = []
for containerinfo in listcontainertowork:
    logging.debug(f"On travaille sur le container {containerinfo}")
    swiftinfocontainer = swiftovh[containerinfo["region"]].head_container(
        containerinfo["name"]
    )

    logging.debug(f"Inforation swift sur le container {swiftinfocontainer}")
    user3 = {}
    if "x-container-read" not in swiftinfocontainer:
        logging.debug("Creation user dedie")
        userinfo = ovhcloud.createuserforcontainer(
            containerinfo["id"], containerinfo["name"]
        )

        logging.debug(f"Information sur l'utilisateur cree {userinfo}")
        user3 = ovhcloud.s3Credentials(userinfo["id"])
    else:
        userid = myfonctions.getuserid(
            listalluser, swiftinfocontainer["x-container-read"].split(":")[1]
        )
        user3 = ovhcloud.gets3Credentials(userid)

    infostemp = {
        "endpoint": "https://s3."
        + str.lower(containerinfo["region"])
        + ".cloud.ovh.net",
        "region": str.upper(containerinfo["region"]),
        "bucket": containerinfo["name"],
        "acces_key": user3["access"],
        "secret": user3["secret"],
    }

    informationsglobales.append(infostemp)
    swiftinfocontainer = swiftovh[containerinfo["region"]].head_container(
        containerinfo["name"]
    )
    swiftovh[containerinfo["region"]].put_container(containerinfo["name"] + "+segments")
    metadata = {
        "x-container-write": swiftovh[containerinfo["region"]].getWriteAcl(
            swiftinfocontainer
        ),
        "x-container-read": swiftovh[containerinfo["region"]].getReadAcl(
            swiftinfocontainer
        ),
    }
    swiftovh[containerinfo["region"]].post_container(
        containerinfo["name"] + "+segments", metadata
    )


print(json.dumps(informationsglobales, indent=4, sort_keys=True))
