FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

# hadolint ignore=DL3005,DL3008
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
    locales \
    python3-pip \
    python3-swiftclient \
    && rm -rf /var/lib/apt/lists/*
    

RUN sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \   
    echo 'LANG="fr_FR.UTF-8"'>/etc/default/locale&& \
    set -eux && dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=fr_FR.UTF-8 && \
    rm /etc/localtime && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime && \
    set -eux && dpkg-reconfigure -f noninteractive tzdata
   
   
ENV LANG fr_FR.utf8
ENV PIP_DEFAULT_TIMEOUT=100


RUN pip3 install --no-cache-dir ovh==0.5.0

COPY *.py /opt/ovh-container-s3/
WORKDIR /opt/ovh-container-s3

CMD [ "sleep", "36000" ]
